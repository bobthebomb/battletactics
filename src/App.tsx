import { Routes, Route } from "react-router-dom";
import "./App.css";
import BattleTacticsApp from "./BattleTacticsApp";
import { BattleTacticsProvider } from "./contexts/battleTactics.context";

function App() {
  return (
    <BattleTacticsProvider>
      <Routes>
        <Route path="/" element={<BattleTacticsApp/>} />
        <Route
          path="/player2"
          element={<BattleTacticsApp/>}
        />
      </Routes>
    </BattleTacticsProvider>
  );
}

export default App;
