import React, { ChangeEvent, useContext } from "react";
import {
  AppBar,
  Grid,
  Paper,
  Toolbar,
  Typography,
  Select,
  Box,
  MenuItem,
  InputLabel,
  FormControl,
  Button,
  SelectChangeEvent,
} from "@mui/material";

import BattleTacticsList from "./BattleTacticsList";
import LanguageOutlinedIcon from "@mui/icons-material/LanguageOutlined";
import ConfirmReset from "./ConfirmReset";
import useToggle from "./hooks/useToggle";
import BattletomeSelectionModal from "./BattletomeSelectionModal";
import { NavLink } from "react-router-dom";
import { BattleTactics } from "./contexts/battleTactics.context";
import { useChangePlayer } from "./hooks/useChangePlayer";
import { ReducerActionPossible } from "./dataTypes/reducerActions";
import { LanguageType } from "./dataTypes/language";

function BattleTacticsApp() {
  const { langue, dispatch, setLangue, book } = useContext(BattleTactics);
  const [confirm, setConfirm] = useToggle(false);
  const [activePlayer, setActivePlayer] = useChangePlayer();

  const handleChangeLangue = (e: SelectChangeEvent<string>) => {
    dispatch({
      type: ReducerActionPossible.ChangeLangue,
      langue: e.target.value,
      battletome: book,
    });
    setLangue(e);
  };

  const handleConfirmReset = () => {
    setConfirm();
  };

  const handleClickPlayer2 = () => {
    setActivePlayer(2);
    dispatch({
      type: ReducerActionPossible.ChangePlayer,
      localStorageKey: "battleTacticsplayer2",
    });
  };

  const handleClickPlayer1 = () => {
    setActivePlayer(1);
    dispatch({
      type: ReducerActionPossible.ChangePlayer,
      localStorageKey: "battleTactics",
    });
  };

  const styleActive = (
    player: number
  ): { margin: string; color?: string; border?: string } => {
    if (player === activePlayer) {
      return { margin: "0.3rem", color: "green", border: "1px solid green" };
    } else {
      return { margin: "0.3rem" };
    }
  };

  return (
    <Paper sx={{ background: "#f0e7d8", minHeight: "100vh" }}>
      <AppBar
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "80px",
          backgroundColor: "#f0e7d8",
        }}
        position="static"
      >
        <Toolbar
          sx={{
            width: "90%",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography variant="h6" sx={{ color: "black" }}>
            Battle Tactics Assistant
          </Typography>

          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <FormControl>
              <InputLabel id="langue">
                <LanguageOutlinedIcon fontSize="small" />
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={langue}
                label="langue"
                onChange={handleChangeLangue}
              >
                <MenuItem value={LanguageType.english}>ENG</MenuItem>
                {/* <MenuItem value={LanguageType.french}>FR</MenuItem>
                <MenuItem value={LanguageType.korean}>KOR</MenuItem> */}
              </Select>
            </FormControl>

            <BattletomeSelectionModal />
          </Box>
        </Toolbar>
      </AppBar>
      <Grid container justifyContent="center">
        <Grid item xl={4} md={8} xs={11}>
          <Paper
            sx={{
              elevation: "7",
              backgroundColor: "#FCF5E5",
              display: "flex",
              justifyContent: "space-evenly",
              margin: "0.5rem 0 ",
            }}
          >
            <NavLink style={{ textDecoration: "none" }} to="/">
              <Button
                onClick={handleClickPlayer1}
                sx={() => styleActive(1)}
                variant="outlined"
              >
                Player 1
              </Button>
            </NavLink>
            <NavLink style={{ textDecoration: "none" }} to="/player2">
              {" "}
              <Button
                onClick={handleClickPlayer2}
                sx={() => styleActive(2)}
                variant="outlined"
              >
                {" "}
                Player 2
              </Button>
            </NavLink>
          </Paper>
          <BattleTacticsList />
        </Grid>
      </Grid>

      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <ConfirmReset confirm={confirm} setConfirm={handleConfirmReset} />
        <Button
          variant="outlined"
          sx={{
            margin: "1rem 0",
            width: "50%",
            height: "60px",
            display: "flex",
            justifyContent: "center",
          }}
          onClick={handleConfirmReset}
        >
          <Typography color="error"> Reset Tactics</Typography>
        </Button>
      </Box>
    </Paper>
  );
}

export default BattleTacticsApp;
function createTheme(arg0: { palette: { secondary: { main: string } } }) {
  throw new Error("Function not implemented.");
}
