import { useLocation } from "react-router-dom";
import { useReducer, useEffect } from "react";
import { oneTactic } from "../interfaces/battletomeInterface";
import { CombinedAction } from "../interfaces/reducerInterface";

type ReducerType = (state: oneTactic[], action: CombinedAction) => oneTactic[];

function useLocalReducer(
  key: string,
  initialval: oneTactic[],
  reducer: ReducerType
) {
  //get the player name from the useLocation hook, then slice the / then concat slicedparams and key to localstorage
  // so i get localstorage key : version, extention, tactics +player2 if player2
  let urlParams = useLocation();
  const slicedparams: string = urlParams.pathname.slice(1);
  const CURRENT_VERSION = 1;
  const localStorageKey: string = key + slicedparams;

  //make piece of state based of value in localstorage
  const [state, dispatch] = useReducer(reducer, initialval, () => {
    let val: oneTactic[];

    const storedVersion = parseInt(localStorage.getItem("data-version") || "0");

    if (storedVersion !== CURRENT_VERSION) {
      localStorage.clear();
      localStorage.setItem("data-version", CURRENT_VERSION.toString());
    }

    try {
      val = JSON.parse(
        window.localStorage.getItem(localStorageKey) ||
          JSON.stringify(initialval)
      );
    } catch (error) {
      console.log("error in the useLocal Reducer hooks" + error);
      val = initialval;
    }
    return val;
  });

  //when state change, save to local storage
  useEffect(() => {
    window.localStorage.setItem(localStorageKey, JSON.stringify(state));
  }, [state]);

  return [state, dispatch] as const;
}

export { useLocalReducer };
