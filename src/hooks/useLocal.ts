import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

function useLocal(key: string, initialval: string) {
  //get the player name from the useLocation hook, then slice the / then concat slicedparams and key to localstorage
  // so i get localstorage key : version, extention, tactics +player2 if player2
  let urlParams = useLocation();

  const slicedparams: string = urlParams.pathname.slice(1);
  const localStorageKey: string = key + slicedparams;
  //make piece of state based of value in localstorage
  const [state, setState] = useState(() => {
    let val: string;
    try {
      val = JSON.parse(
        window.localStorage.getItem(localStorageKey) || `"${initialval}"`
      );
    } catch (error) {
      console.log("error catch : " + error);
      val = initialval;
    }

    return val;
  });

  // Quand l'url Params change en passe d'un player 1 a un player 2 le useEffect se lance. Il se lance également sur le component didmount meme si l'URL params ne change pas.
  //Si le Parse trouve une ref du player s (book, langue, result) la function  set le state avec le hook comme ca le useEffect 2 va save le state actuel dans le local storage pour le joueur 2 (ou inversement)
  //Mais si le Parse ne trouve pas de ref (revoie null) setstate avec l'initialVal (ENG, No battletome, 8tactics vide) et l'ajouter au local storage.
  //il est important de faire le setItem ici meme si y'a le deuxieme useEffect sinon come le state reste le meme il ne save au local storage.

  useEffect(() => {
    const savedPlayerRef: string = JSON.parse(
      window.localStorage.getItem(localStorageKey) || '"false"'
    );

    if (savedPlayerRef !== "false") {
      setState(savedPlayerRef);
    } else {
      setState(initialval);
      window.localStorage.setItem(localStorageKey, JSON.stringify(initialval));
    }
  }, [urlParams]);

  // use useEffect to update local if state change, initialval must exist

  useEffect(() => {
    initialval &&
      window.localStorage.setItem(localStorageKey, JSON.stringify(state));
  }, [state]);

  const reset = () => {
    setState(initialval);
  };

  return [state, setState, reset] as const;
}
export default useLocal;
