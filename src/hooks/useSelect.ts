import { SelectChangeEvent } from "@mui/material";
import useLocal from "./useLocal";
import { LanguageType } from "../dataTypes/language";
// i keep useSelect hook for langue and battletome spread in the context. Alos still use useLocal custom hook for remanence
//next interation of the app will remove these..

function useSelect(key: string, initialValue: string) {
  const [state, setState] = useLocal(key, initialValue);

  const changeSelect = (e: SelectChangeEvent<string>) => {
    setState(e.target.value);
  };

  return [state, changeSelect] as const;
}

export default useSelect;
