import { useState } from "react";

export const useChangePlayer = (player: number = 1) => {
  const [playerActive, setPlayerActive] = useState(player);

  const changePlayer = (player: number): void => {
    setPlayerActive(player);
  };

  return [playerActive, changePlayer] as const;
};
