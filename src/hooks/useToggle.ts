import { useState } from "react";

function useToggle(initialVal = false) {
  const [state, setState] = useState(initialVal);

  const toggle = (): void => {
    setState(!state);
  };

  return [state, toggle] as const;
}
export default useToggle;
