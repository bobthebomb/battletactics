//initialised in the battleTacticsApp.js component
import { oneTactic } from "../interfaces/battletomeInterface";
import { CombinedAction } from "../interfaces/reducerInterface";
import { ReducerActionPossible } from "../dataTypes/reducerActions";
import {
  battleTacticsBattletome,
  objOfTacticsVersion,
} from "../realmsTactics/AllTacticsVersion";

export const battletacticsReducer = (
  state: oneTactic[],
  action: CombinedAction
): oneTactic[] => {
  switch (action.type) {
    case ReducerActionPossible.ToggleTactics:
      const changedTactics = state.map((tactic) => {
        if (tactic.id === action.id) {
          return { ...tactic, result: action.r };
        } else {
          return { ...tactic };
        }
      });
      return changedTactics;
    //BEWARE !!! ChangeLangue only translate description and name of the battletactics array. I still use an Hook to share langue context

    case ReducerActionPossible.ChangeLangue:
      const battlestacticsWithLangue: oneTactic[] =
        objOfTacticsVersion[action.langue as keyof typeof objOfTacticsVersion];

      const slicedTactics: oneTactic[] = state.slice(0, 8);
      const translatedBattletactics: oneTactic[] = slicedTactics.map(
        (tactic, idx) => {
          return {
            ...tactic,
            name: battlestacticsWithLangue[idx].name,
            description: battlestacticsWithLangue[idx].description,
          };
        }
      );

      // i must add the correct langue battletome if possible
      const changeLangueBattletome =
        action.battletome === "NO"
          ? []
          : battleTacticsBattletome[
              (action.battletome +
                action.langue) as keyof typeof battleTacticsBattletome
            ];

      return [...translatedBattletactics, ...changeLangueBattletome];

    //BEWARE !!! ChangeBattletome only add the battletactics . I still use an Hook to share book context

    case ReducerActionPossible.ChangeBattletome:
      //"NO" is when no battletome is chosen
      const TacticsFromGur = state.slice(0, 8);
      const BattleTome =
        action.battletome === "NO"
          ? []
          : battleTacticsBattletome[
              (action.battletome +
                action.langue) as keyof typeof battleTacticsBattletome
            ];
      return [...TacticsFromGur, ...BattleTome];

    case ReducerActionPossible.Reset:
      const resetedTactics = state.map((tactic) => {
        return { ...tactic, result: "ongoing" };
      });

      return resetedTactics;

    // when i change player i fetch from the local storage. and if there is nothing i setItem with the basic english ghur
    case ReducerActionPossible.ChangePlayer:
      let savedPlayerRef: string | null = "";
      try {
        if (window.localStorage.getItem(action.localStorageKey)) {
          savedPlayerRef = window.localStorage.getItem(action.localStorageKey);
        }
      } catch (error) {
        console.log(`error from the try in the reducer with code :  ${error}`);
      }

      const otherPlayerBattleTactics: oneTactic[] = savedPlayerRef
        ? JSON.parse(savedPlayerRef)
        : [...objOfTacticsVersion["ENG"]];

      return otherPlayerBattleTactics;

    default:
      return state;
  }
};
