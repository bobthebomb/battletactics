//map autour de result success ajoute 2 points si c'est bon on va faire une fonction qui prend un argument genre l'Array des resultats et on va return le count

import { oneTactic } from "./interfaces/battletomeInterface";

const theScoring: (lesTactics: oneTactic[]) => number = (
  lesTactics: oneTactic[]
): number => {
  let score = 0;
  lesTactics.map((tactic) => {
    if (tactic.result === "success") {
      score += 2;
    }
  });
  return score;
};

export { theScoring };
