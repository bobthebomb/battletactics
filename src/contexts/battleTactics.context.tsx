//track battletactics
import React, { createContext } from "react";
import useSelect from "../hooks/useSelect";
import { battletacticsReducer } from "../reducers/battleTactics.reducer";
import { useLocalReducer } from "../hooks/useLocalReducer";
import {
  battleTacticsBattletome,
  objOfTacticsVersion,
} from "../realmsTactics/AllTacticsVersion";
import { oneTactic } from "../interfaces/battletomeInterface";
import { CombinedAction } from "../interfaces/reducerInterface";
import { SelectChangeEvent } from "@mui/material";
import { LanguageType } from "../dataTypes/language";

interface BTProvider {
  children: JSX.Element;
}

interface ForMyCreateContext {
  book: string;
  langue: string;
  tactics: oneTactic[];
  dispatch: React.Dispatch<CombinedAction>;
  setLangue: (e: SelectChangeEvent<string>) => void;
  setBook: (e: SelectChangeEvent<string>) => void;
}

export const BattleTactics = createContext<ForMyCreateContext>(
  {} as ForMyCreateContext
);

export function BattleTacticsProvider(props: BTProvider) {
  const [book, setBook] = useSelect("expension", "NO");
  const [langue, setLangue] = useSelect("version", LanguageType.english);
  const initialValTest = [...objOfTacticsVersion["ENG"]];
  const [tactics, dispatch] = useLocalReducer(
    "battleTactics",
    initialValTest,
    battletacticsReducer
  );

  return (
    <BattleTactics.Provider
      value={{ tactics, dispatch, langue, setLangue, book, setBook }}
    >
      {props.children}
    </BattleTactics.Provider>
  );
}
