import React from "react";
import {
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Typography,
  SelectChangeEvent,
} from "@mui/material";
import MenuBookRoundedIcon from "@mui/icons-material/MenuBookRounded";
import { battletomeMenuList } from "./realmsTactics/AllTacticsVersion";
import { BattleTactics } from "./contexts/battleTactics.context";
import { useContext } from "react";
import { ReducerActionPossible } from "./dataTypes/reducerActions";

function BattletomeSelectionModal() {
  const { book, langue, setBook, dispatch } = useContext(BattleTactics);

  //i use e for setBook hook because i have e.target.value in the custom hook
  const handleChange = (e: SelectChangeEvent<string>) => {
    setBook(e);
    dispatch({
      type: ReducerActionPossible.ChangeBattletome,
      langue: langue,
      battletome: e.target.value,
    });
  };

  const theMenu = battletomeMenuList.map((faction) => {
    return (
      // key is homemade because of chain rerender, also the Typography is here to reduce the length of the name in the button but render the full name when the menu is open
      // if book context ===  faction.name render only first 6 char if not render full.
      <MenuItem key={faction.val + faction.name} value={faction.val}>
        <Typography variant="caption">
          {book === faction.val
            ? `${faction.name.substring(0, 6)}...`
            : faction.name}
        </Typography>
      </MenuItem>
    );
  });

  return (
    <FormControl>
      <InputLabel id="book">
        <MenuBookRoundedIcon fontSize="small" />{" "}
      </InputLabel>
      <Select
        labelId="book-label"
        id="book"
        value={book}
        label="book"
        onChange={handleChange}
      >
        <MenuItem value="NO">
          <Typography variant="caption">Battletome</Typography>
        </MenuItem>

        {theMenu}
      </Select>
    </FormControl>
  );
}
export default BattletomeSelectionModal;
