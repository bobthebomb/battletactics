export enum ReducerActionPossible {
    ToggleTactics = "ToggleTactics",
    ChangeLangue = "ChangeLangue",
    ChangeBattletome = "ChangeBattletome",
    Reset = "Reset",
    ChangePlayer = "ChangePlayer",
  }
  