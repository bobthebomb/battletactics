export enum LanguageType {
    english = "ENG",
    french = "FR",
    korean = "KOR"
}