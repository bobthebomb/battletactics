// battletactics can have 3 state : "success" "ongoing" "failure"
// i did a type would it work with an enum ?

export type NbattleTacticResult = "success" | "ongoing" | "failure";

export enum battleTacticResult {
  win = "success",
  failed = "failure",
  notAttempted = "ongoing",
}
