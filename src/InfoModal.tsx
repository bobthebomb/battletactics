import React, { Component } from "react";
import Modal from "@mui/material/Modal";
import { Typography, Box } from "@mui/material";

interface InfoModalProps {
  open: boolean;
  handleClose: Function;
  tactic: {
    name: string;
    description: string;
  };
}

function InfoModal(props: InfoModalProps): JSX.Element {
  const { open, handleClose } = props;

  const { name, description } = props.tactic;

  const style = {
    position: "absolute",
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "60%",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Modal
      open={open}
      onClose={() => handleClose()}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          {name}
        </Typography>
        <Typography
          variant="caption"
          id="modal-modal-description"
          sx={{ mt: 2 }}
        >
          {description}
        </Typography>
      </Box>
    </Modal>
  );
}
export default InfoModal;
