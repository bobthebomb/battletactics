import { ReducerActionPossible } from "../dataTypes/reducerActions";
import { battleTacticResult } from "../dataTypes/battleTacticsResult";

interface ActionToggleTactics {
  type: ReducerActionPossible.ToggleTactics;
  r: battleTacticResult;
  id: number;
}

interface ActionChangeLangue {
  type: ReducerActionPossible.ChangeLangue;
  langue: string;
  battletome: string;
}

interface ActionChangeBattletome {
  type: ReducerActionPossible.ChangeBattletome;
  langue: string;
  battletome: string;
}

interface ActionResetBattleTactics {
  type: ReducerActionPossible.Reset;
}

interface ActionChangePlayer {
  type: ReducerActionPossible.ChangePlayer;
  localStorageKey: string;
}

export type CombinedAction =
  | ActionToggleTactics
  | ActionChangeLangue
  | ActionChangeBattletome
  | ActionResetBattleTactics
  | ActionChangePlayer;
