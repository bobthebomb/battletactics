export interface oneTactic {
  id: number;
  rank: number;
  name: string;
  description: string;
  monsterBonus: boolean;
  result: string;
}
