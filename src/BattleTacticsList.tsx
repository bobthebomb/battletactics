import React, { useContext } from "react";
import BattleTactic from "./BattleTactic";
import { List, Box, Divider, Paper } from "@mui/material";
import { BattleTactics } from "./contexts/battleTactics.context";

function BattleTacticsList(): JSX.Element {
  const { tactics } = useContext(BattleTactics);

  // not very clean but should allow inclusion of Battletome specific Battle tactics.

  const nonCompltedTactics = tactics.filter(
    (tactic) => tactic.result === "ongoing"
  );
  const completedTactics = tactics.filter(
    (tactic) => tactic.result !== "ongoing"
  );
  // const sortedTactics = [...nonCompltedTactics, ...completedTactics]

  const renderNonCompleted = nonCompltedTactics.map((tactic, idx) => (
    <Box key={idx}>
      <BattleTactic tactic={tactic} />
      {tactics.length - 1 > idx && <Divider />}
    </Box>
  ));

  const rendercompletedTactics = completedTactics.map((tactic, idx) => (
    <Box key={idx}>
      <BattleTactic tactic={tactic} />
      {tactics.length - 1 > idx && <Divider />}
    </Box>
  ));

  return (
    <Box>
      <Paper elevation={7} sx={{ backgroundColor: "#FCF5E5" }}>
        <List>{renderNonCompleted}</List>
      </Paper>

      {completedTactics.length > 0 ? (
        <Paper
          elevation={7}
          sx={{ marginTop: "1rem", backgroundColor: "#FCF5E5" }}
        >
          <List>{rendercompletedTactics}</List>
        </Paper>
      ) : null}
    </Box>
  );
}
export default BattleTacticsList;
