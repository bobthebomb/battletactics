import React, { useContext } from "react";
import { ListItem, ListItemText, Typography, IconButton } from "@mui/material";
import PlusOneRoundedIcon from "@mui/icons-material/PlusOneRounded";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import MonsterBonusModal from "./MonsterBonusModal";
import useToggle from "./hooks/useToggle";
import InfoModal from "./InfoModal";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import HourglassTopRoundedIcon from "@mui/icons-material/HourglassTopRounded";
import CheckCircleOutlineRoundedIcon from "@mui/icons-material/CheckCircleOutlineRounded";
import HighlightOffRoundedIcon from "@mui/icons-material/HighlightOffRounded";
import { BattleTactics } from "./contexts/battleTactics.context";
import { oneTactic } from "./interfaces/battletomeInterface";
import { ReducerActionPossible } from "./dataTypes/reducerActions";
import { battleTacticResult } from "./dataTypes/battleTacticsResult";

interface BtProps {
  tactic: oneTactic;
}

function BattleTactic(props: BtProps) {
  const { dispatch } = useContext(BattleTactics);
  const { name, monsterBonus, result, id } = props.tactic;
  const [openMonster, setOpenMonster] = useToggle();
  const [openInfo, setOpenInfo] = useToggle(false);

  const handleToggleMonster = () => {
    setOpenMonster();
  };

  const handleToggleInfo = () => {
    setOpenInfo();
  };

  const handleResultChange = (
    e: React.MouseEvent<HTMLElement, MouseEvent>,
    newResult: battleTacticResult
  ) => {
    // e.target.value ne fonctionne pas avec se MenuSelect de MUI, donc faut utiliser le deuxieme params.
    //Le deuxieme params newResult est null quand tu clicks sur le meme boutton (renvoie null) faudrait chercher pourquoi .
    //dans l'état je vais juste prevent setTactics(newresult, id) si newResult e st   null
    if (newResult) {
      dispatch({
        type: ReducerActionPossible.ToggleTactics,
        r: newResult,
        id: id,
      });
    }
  };

  type Colortest = "success" | "error" | "primary";

  const coloring = (): Colortest => {
    if (result === "success") {
      return "success";
    } else if (result === "failure") {
      return "error";
    } else {
      return "primary";
    }
  };

  const coloringCss = () => {
    if (result === "success") {
      return "green";
    } else if (result === "failure") {
      return "red";
    } else {
      return "blue";
    }
  };

  const control = {
    color: coloring(),
    value: result,
    exclusive: true,
  };

  return (
    <ListItem>
      <MonsterBonusModal
        name={name}
        handleClose={handleToggleMonster}
        open={openMonster}
      />

      <InfoModal
        tactic={props.tactic}
        handleClose={handleToggleInfo}
        open={openInfo}
      />

      <ToggleButtonGroup
        sx={{ marginRight: "0.3rem" }}
        onChange={(
          e: React.MouseEvent<HTMLElement, MouseEvent>,
          newResult: battleTacticResult
        ) => handleResultChange(e, newResult)}
        {...control}
      >
        <ToggleButton value={battleTacticResult.win} size="small">
          <CheckCircleOutlineRoundedIcon />
        </ToggleButton>
        <ToggleButton value={battleTacticResult.notAttempted} size="small">
          <HourglassTopRoundedIcon />
        </ToggleButton>
        <ToggleButton value={battleTacticResult.failed} size="small">
          <HighlightOffRoundedIcon />
        </ToggleButton>
      </ToggleButtonGroup>

      <ListItemText
        sx={{
          textDecoration:
            result !== "ongoing" ? "line-through solid 3px" : null,
          textDecorationColor: coloringCss,
        }}
      >
        <Typography variant="caption"> {name}</Typography>
      </ListItemText>

      <IconButton
        aria-label="monsterIcon"
        onClick={handleToggleMonster}
        sx={{ padding: "0px", margin: "0px" }}
      >
        {monsterBonus ? (
          <PlusOneRoundedIcon sx={{ color: coloringCss, padding: "0px" }} />
        ) : null}
      </IconButton>

      <IconButton
        aria-label="HelpOutlineIcon"
        onClick={handleToggleInfo}
        sx={{ padding: "0px", margin: "0px" }}
      >
        <HelpOutlineIcon sx={{ padding: "0px" }} />
      </IconButton>
    </ListItem>
  );
}
export default BattleTactic;
