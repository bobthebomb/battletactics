import { oneTactic } from "../interfaces/battletomeInterface";


const feedDescription = 'You complete this tactic if at least 7 enemy models are slain by disease rolls during this turn.'
const nurtureDescription = 'Pick 1 Feculent Gnarlmaw in your army that is within 12" of any enemy units. You complete this tactic if that Feculent Gnarlmaw is more than 12" from all enemy units at the end of this turn.'
const giftsDescription = 'You complete this tactic if 3 or more friendly MAGGOTKIN OF NURGLE units are within 3" of the same enemy unit at the end of this turn.'
const gloryDescription = 'You complete this tactic at the end of this turn if more enemy units than friendly units are destroyed during this turn.'
const drowningDescription = 'You complete this tactic if there is a different friendly unit with a Rot Fly mount in each quarter of the battlefield at the end of this turn.'
const suddenDomminationDescription = 'You complete this tactic if you summon a GREAT UNCLEAN ONE to the battlefield during this turn and it is within 3" of an objective that you control in your opponent’s territory at the end of this turn.'


const maggotKinTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Feed the Maggots", description: feedDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Nurture the Gnarlmaw", description: nurtureDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Gifts of Nurgle", description: giftsDescription , monsterBonus: false, result : "ongoing" },
    {id : 12 , rank: 12,  name : "Glory to the Grandfather!", description: gloryDescription , monsterBonus: false, result : "ongoing" },
    {id : 13 , rank: 13,  name : "The Droning", description: drowningDescription , monsterBonus: false, result : "ongoing" },
    {id : 14 , rank: 14,  name : "Sudden Domination", description: suddenDomminationDescription , monsterBonus: false, result : "ongoing" },

]

const feedDescriptionKOR = '이번 턴 안에 "disease" 굴림으로 적 모델을 최소 7 모델 정도 제거했다면, 이 배틀 택틱을 달성한다.'
const nurtureDescriptionKOR =  '12" 이내에 적 유닛이 있는 자신에 아미에 속한 Feculent Gnarlmaw를 하나 선택한다. 이번 턴이 끝날 때, 해당 Feculent Gnarlmaw 12" 이내에 적 유닛이 없으면 이 배틀 택틱을 달성한다.'
const giftsDescriptionKOR = '이번 턴 안에 3 이상의 아군 MAGGOTKIN OF NURGLE 유닛이 같은 적 유닛 3" 이내에 들어가 있다면, 이 배틀 택틱을 달성한다.'
const gloryDescriptionKOR =  '이번 턴 안에 적 유닛이 아군 유닛보다 많이 파괴되었다면, 이 배틀 택틱을 달성한다.'
const drowningDescriptionKOR =  '이번 턴이 끝날 때, Rot Fly 마운트를 탑승한 각기 다른 아군 유닛이 전장의 각 사분면마다 있다면, 이 배틀 택틱을 달성한다.'
const suddenDomminationDescriptionKOR = '이번 턴 안에 GREAT UNCLEAN ONE을 전장에 소환하고, 해당 유닛이 이번 턴이 끝날 때 상대방 영역의 오브젝트 3" 이내에 있다면, 이 배틀 택틱을 달성한다.'


const maggotKinTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Feed the Maggots(kr)", description: feedDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Nurture the Gnarlmaw(kr)", description: nurtureDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Gifts of Nurgle(kr)", description: giftsDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 12 , rank: 12,  name : "Glory to the Grandfather!(kr)", description: gloryDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 13 , rank: 13,  name : "The Droning(kr)", description: drowningDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 14 , rank: 14,  name : "Sudden Domination(kr)", description: suddenDomminationDescriptionKOR , monsterBonus: false, result : "ongoing" },

]

export {maggotKinTactics, maggotKinTacticsKOR} ; 