import { oneTactic } from "../interfaces/battletomeInterface";

const clashOfArmsDescription =
  "You complete this tactic if 3 or more friendly units make a charge move this turn. if 2 or more of those units are WITCH AELVES or SISTERS OF SLAUGHTER score one extra point";
const cruelDelightDescription =
  "You complete this tactic if 2 or more friendly Khinerai units move using their Fire and Flight ability or Fight and Flight ability this turn.";
const tideOfBladeDescription =
  "You complete this tactic if there are 2 or more units from your starting army wholly within your opponents teritory at the end of this turn. If 2 or more of thos units are WITCH AELVES score 1 additionnal victory point.";
const executionnerCultDescription =
  "You can pick this battle tactic only if there is a friendly HIGH GLADIATRIX on the battelfied. You complete this tactic if an enemy HERO is slain by that unit's Killin Stroke ability this turn.";
const hatredOfChaosDescription =
  "You can pick this battle tactic only if you have a Hagg Nar or Khelt Nar army. You complete this tactic if 2 or more Chaos units are destroyed this turn.";
const unexpectedAttackDescription =
  "You completthis tactic if a friendly KHAINITE SHADOWSTALKERS unit uses it's Shadow Leap ability and makes a charge move this turn";

const dokTactics: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "clash of Arms",
    description: clashOfArmsDescription,
    monsterBonus: true,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "Cruel Delight",
    description: cruelDelightDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Hammerstrike Assault",
    description: tideOfBladeDescription,
    monsterBonus: true,
    result: "ongoing",
  },
  {
    id: 12,
    rank: 12,
    name: "Executionner's Cult",
    description: executionnerCultDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 13,
    rank: 13,
    name: "Hatred of Chaos",
    description: hatredOfChaosDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 14,
    rank: 14,
    name: "Unexpected Attack",
    description: unexpectedAttackDescription,
    monsterBonus: false,
    result: "ongoing",
  },
];
export { dokTactics };
