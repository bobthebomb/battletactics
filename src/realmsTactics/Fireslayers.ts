import { oneTactic } from "../interfaces/battletomeInterface";

const grudgeDescription =
  'Each time a friendly unit is destryoed by wounds caused by an attack, make a note of the enemy unit that made that attack(in a "Book of Grudges", if you wish). When you pick this battle tactic, pick 1 of those enemy units that is still on the battlefield. You complete this tactic if that unit is destroyed during this turn';
const beastslayerDescription =
  "Pick 1 enemy Monster and 1 friendly Hero. You complete this tactic if that MONSTER is slain by wounds caused by attacks made by that Hero during this turn";
const grimnirDescription =
  'You complete this tactic at the end of this turn if there are any friendly VULKITE BERZERKERS units on the battlefield and all of them are within 3" of any enemy units.';
const honourableDeathDescription =
  "Pick 1 friendly HERO. You complete this tactic if that friendly HERO is slain during this turn and any enemy models were also slain by wounds caused by attacks made by that HERO during this turn";
const seizeByForceDescription =
  "You can pick this tactic only if you control fewer objectives than your opponent. You complete this tactic if you control more objectives than your opponent at the end of this turn.";
const ingnominiousDescription =
  "Pick 1 enemy HERO. You complete this tactic if that enemy HERO is slain by wounds caused by an attack made with Fyresteel Throwing Axe during this turn.";

const fireslayersTactics: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Settle a Grudge",
    description: grudgeDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "Beastslayer",
    description: beastslayerDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Grimnir Knows No Mercy",
    description: grimnirDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 12,
    rank: 12,
    name: "An honourable Death",
    description: honourableDeathDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 13,
    rank: 13,
    name: "Seize by Force",
    description: seizeByForceDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 14,
    rank: 14,
    name: "An ignominious Death",
    description: ingnominiousDescription,
    monsterBonus: false,
    result: "ongoing",
  },
];

const grudgeDescriptionKOR =
  '아군 유닛이 공격에 의해 파괴될 떄마다, 해당 공격을 가한 적 유닛을 하나 선택해 기록한다. (원한다면, "원한의 고서"에 기록하라). 이 배틀 택틱을 선택할 때, 전장의 해당 적 유닛 중 하나를 선택한다. 이번 턴 안에 해당 적 유닛을 파괴하면, 이 배틀 택틱을 달성한다.';
const beastslayerDescriptionKOR =
  "아군 HERO랑 적 MONSTER를 하나 선택한다. 이번 턴 안에 해당 HERO가 가한 공격으로 MONSTER를 제거하면, 이 배틀 택틱을 달성한다.";
const grimnirDescriptionKOR =
  '이번 턴이 끝날 때, 전장의 모든 아군 VULKITE BERZERKERS가 적 3" 이내에 있다면, 이 배틀 택틱을 달성한다.';
const honourableDeathDescriptionKOR =
  "아군 HERO를 하나 선택한다. 이번 턴 안에 해당 HERO가 제거되고, 해당 HERO가 가한 공격에 적 모델이 제거되어 있다면 이 배틀 택틱을 달성한다.";
const seizeByForceDescriptionKOR =
  "이 배틀 택틱은 오직 상대방보다 오브젝트를 적게 점령하고 있을 때만 선택할 수 있다. 이번 턴이 끝날 때, 상대방보다 많은 숫자의 오브젝트를 점령하면 달성한다.";
const ingnominiousDescriptionKOR =
  "적 HERO를 하나 선택한다. 이번 턴 안에 해당 적 HERO를 Fyresteel Throwing Axe 공격으로 제거한다면, 이 배틀 택틱을 달성한다.";

const fireslayersTacticsKOR: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Settle a Grudge(kr)",
    description: grudgeDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "Beastslayer(kr)",
    description: beastslayerDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Grimnir Knows No Mercy(kr)",
    description: grimnirDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 12,
    rank: 12,
    name: "An honourable Death(kr)",
    description: honourableDeathDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 13,
    rank: 13,
    name: "Seize by Force(kr)",
    description: seizeByForceDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 14,
    rank: 14,
    name: "An ignominious Death(kr)",
    description: ingnominiousDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
];
export { fireslayersTactics, fireslayersTacticsKOR };
