import { oneTactic } from "../interfaces/battletomeInterface";

const bannerDescription = 'Pick 2 objective markers on the battlefield that are not wholly within your territory, and wich are more than 12" apart. You complete this battle tactic if a friendly Stndard Bearer model is within 1" of each Objective markers at the end of this turn'
const sanctityDescription = 'Pick 1 friendly FLAGELLANTS unit that is not within 6" of an objective wholly or partially within ennemy territory. You complete this battle tactic if that unit is within 6" of an objective wholly or partially within enemy territory at the end of this turn'


const citiesTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Banner Held High", description: bannerDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Sanctify", description: sanctityDescription , monsterBonus: false, result : "ongoing" },
]

const bannerDescriptionKOR = '전장의 자신의 배치 구역안에 완전히 들어가지 않으며, 서로 12" 이상 떨어진 오브젝트 마커를 2개 선택한다. 이번 턴이 종료될 때, 각 오브젝트 마커 1" 이내에 아군 기수가 있다면 이 전투 전략을 달성한다.'
const sanctityDescriptionKOR = '오브젝트 6" 이내에 들어가지 않거나, 적 배치 구역에 들어가지 않은 아군 FLAGELLANTS 유닛을 하나 선택한다. 이번 턴이 종료될 때, 해당 유닛이 오브젝트 6" 이내로 들어가거나, 적 배치 구역에 들어가면 이 전투 전략을 달성한다.'


const citiesTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Banner Held High(kr)", description: bannerDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Sanctify(kr)", description: sanctityDescriptionKOR , monsterBonus: false, result : "ongoing" },
]

export {citiesTactics, citiesTacticsKOR} ; 