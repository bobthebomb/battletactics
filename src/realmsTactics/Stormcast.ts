import { oneTactic } from "../interfaces/battletomeInterface";

const pionnerDescription =
  "You complete this tactic if all of the objectives wholly within your opponent’s territory are contested by friendly CITIES OF SIGMAR units at the end of this turn.";
const noChallengeDescription =
  "You complete this tactic if an enemy HERO is slain by wounds caused by an attack made with a melee weapon by a friendly REDEEMER unit during this turn.";
const hammerStrikeDescription =
  "Pick 1 HERO in your opponent’s starting army that is on the battlefield, that has a Wounds characteristic of 10 or more and that has 0 wounds allocated to it. You complete this tactic if that HERO is slain during this turn.";
const lightningDescription =
  "Pick 1 unit in your opponent’s starting army that is on the battlefield and that has a Bravery characteristic of 10 or more. You complete this tactic if any models in that unit flee during this turn.";
const draconithDescription =
  "Pick 1 unit in your opponent’s starting army that is on the battlefield and that has 10 or more models. You complete this tactic if that unit is destroyed by wounds caused by attacks made by friendly DRACONITH, STARDRAKE or DRACOTH units during this turn.";
const aMatterofHonourDescription =
  "Pick 1 MONSTER in your opponent’s starting army that is on the battlefield. You complete this tactic if that unit is destroyed by wounds caused by attacks made by friendly DRACONITH or STARDRAKE units during this turn.";

const stormcastEternalsTactics: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Pioneers of the Realms",
    description: pionnerDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "No Challenge Too Great",
    description: noChallengeDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Hammerstrike Assault",
    description: hammerStrikeDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 12,
    rank: 12,
    name: "Lightning-shattered Morale",
    description: lightningDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 13,
    rank: 13,
    name: "Draconith Destruction",
    description: draconithDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 14,
    rank: 14,
    name: "A Matter of Honour",
    description: aMatterofHonourDescription,
    monsterBonus: false,
    result: "ongoing",
  },
];

const pionnerDescriptionKOR =
  "이번 턴이 끝날 때, 상대방 영역에 완전히 들어가 있는 모든 오브젝트에 아군 CITIES OF SIGMAR 유닛이 경합하고 있다면, 이 배틀 택틱을 달성한다.";
const noChallengeDescriptionKOR =
  "이번 턴 안에 아군 REDEEMER 유닛의 공격으로 적 HERO가 제거되면, 이 배틀 택틱을 달성한다.";
const hammerStrikeDescriptionKOR =
  "상대방의 시작 아미에 포함되었으며, 운드 수치가 10 이상이고 아직 피해를 입지 않은 전장의 HERO를 하나 선택한다. 해당 히어로를 이번 턴 안에 제거하면, 이 배틀 택틱을 달성한다.";
const lightningDescriptionKOR =
  "상대방의 시작 아미에 포함되었으며, 브레이버리 수치가 10 이상인 전장의 유닛을 하나 선택한다. 해당 유닛이 이번 턴 안에 도주하면, 이 배틀 택틱을 달성한다.";
const draconithDescriptionKOR =
  "상대방 시작 아미에 포함되었으며, 10 이상의 모델로 구성된 전장의 유닛을 하나 선택한다. 해당 유닛이 이번 턴 안에 아군 DRACONITH, STARDRAKE 혹은 DRACOTH 유닛의 공격으로 파괴되면, 이 배틀 택틱을 달성한다.";
const aMatterofHonourDescriptionKOR =
  "상대방 시작 아미에 포함된 전장의 MONSTER 유닛을 하나 선택한다. 해당 유닛이 이번 턴 안에 아군 DRACONITH, STARDRAKE의 공격으로 파괴되면, 이 배틀 택틱을 달성한다.";

const stormcastEternalsTacticsKOR: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Pioneers of the Realms(kr)",
    description: pionnerDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "No Challenge Too Great(kr)",
    description: noChallengeDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Hammerstrike Assault(kr)",
    description: hammerStrikeDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 12,
    rank: 12,
    name: "Lightning-shattered Morale(kr)",
    description: lightningDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 13,
    rank: 13,
    name: "Draconith Destruction(kr)",
    description: draconithDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 14,
    rank: 14,
    name: "A Matter of Honour(kr)",
    description: aMatterofHonourDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
];

export { stormcastEternalsTactics, stormcastEternalsTacticsKOR };
