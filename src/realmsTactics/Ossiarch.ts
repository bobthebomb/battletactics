import { oneTactic } from "../interfaces/battletomeInterface";


const trampeDescription = 'Pick 1 friendly KAVALOS DEATHRIDERS unit that is more than 3" from all enemy units. You complete this battle tactic if that unit makes a charge move this turn and is within 3" of any enemy units at the end of this turn '
const unfeelingDescription = 'You complete this battle tactic if two or more friendly OSSIARCH BONEREAPERS units had slain models returned to them with the Reknit Constructs Ossarich command ability in this turn'
const titheDescription = 'Pick 1 enemy Hero or Monster on the battelfied. You complete this battle tactic if that unit is destroyed this turn. IF that nenemy HERO or MONSTER was destroyed by an attack made by a friendly GOTHIZZAR HARVESTER , score 1 additional victory point'


const ossiarchTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Trample the defiant", description: trampeDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Unfeeling recursion", description: unfeelingDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "The tithe demands", description: titheDescription , monsterBonus: true, result : "ongoing" },
]

const trampeDescriptionKOR = '적 3" 바깥의 아군 KAVALOS DEATHRIDERS 유닛을 하나 선택한다. 이번 턴에 해당 유닛이 차지 이동을 하고, 턴 종료 시 적 유닛 3" 이내에 있다면 배틀 택틱을 달성한다.'
const unfeelingDescriptionKOR = '이번 턴 안에 둘 이상의 아군 OSSIARCH BONEREAPERS 유닛이 Reknit Constructs 오시아크 커맨드 어빌리티로 모델을 부활시켰다면, 배틀 택틱을 달성한다.'
const titheDescriptionKOR = '전장의 적 HERO나 MONSTER를 하나 선택한다. 이번 턴 안에 해당 유닛을 파괴하면, 배틀 택틱을 달성한다. 만약, 해당 유닛이 아군 GOTHIZZAR HARVESTER의 공격으로 파괴되었다면 추가로 승점을 1점 획득한다'


const ossiarchTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Trample the defiant(kr)", description: trampeDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Unfeeling recursion(kr)", description: unfeelingDescriptionKOR, monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "The tithe demands(kr)", description: titheDescriptionKOR , monsterBonus: true, result : "ongoing" },
]

export {ossiarchTactics, ossiarchTacticsKOR } ; 