import { oneTactic } from "../interfaces/battletomeInterface";

const sothsDescription =
  'Pick 1 ennemy unit within 9" of your Herdstone. You complete this battle tactic if that unit is destroyed this turn';
const FuryDescription =
  'You can pick this tactic only in your first turn. You complete this battle tactic if the model picked to be your general and two other friendly BEAST OF CHAOS untis are within 3" of an enemy unit at the end of the turn';
const wrathDescription =
  "Pick 1 objective controlled by your opponent . You complete this battle tactic at the end of the turn if you contral that objective and is it contested by any models in your army that were summoned with Primordial Call Battle Trait";

const BeastofChaosTactics: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Shadow of the HerdStone",
    description: sothsDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "Fury of The Wild",
    description: FuryDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Wrath of the Warped Wilds",
    description: wrathDescription,
    monsterBonus: false,
    result: "ongoing",
  },
];

const sothsDescriptionFR =
  'Choisissez une unité énemie dans les 9" de votre pierre des hardes. Vous completez cette tactique de bataille si cette unité est détruite pendant ce tour';
// '
const FuryDescriptionFR =
  'You can pick this tactic only in your first turn. You complete this battle tactic if the model picked to be your general and two other friendly BEAST OF CHAOS untis are within 3" of an enemy unit at the end of the turn';
const wrathDescriptionFR =
  "Pick 1 objective controlled by your opponent . You complete this battle tactic at the end of the turn if you contral that objective and is it contested by any models in your army that were summoned with Primordial Call Battle Trait";

const BeastofChaosTacticsfr: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Ombre de la pierre des hardes",
    description: sothsDescriptionFR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "Furie sauvage",
    description: FuryDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Rage sauvage",
    description: wrathDescription,
    monsterBonus: false,
    result: "ongoing",
  },
];

const sothsDescriptionKOR =
  '허드스톤 9" 이내의 적 유닛을 하나 선택한다. 해당 유닛이 이번 턴 이내에 파괴되면 이 배틀 택틱을 달성한다.';
const FuryDescriptionKOR =
  '오직 자신의 첫 턴에만 이 배틀 택틱을 선택할 수 있다. 제너럴로 선택된 모델과 2 이상의 다른 아군 BEAST OF CHAOS 유닛이 턴이 종료될 때 적 3" 이내에 있다면 이 배틀 택틱을 달성한다.';
const wrathDescriptionKOR =
  "상대방이 점령하고 있는 오브젝트를 하나 선택한다. 이번 턴이 종료될 때 해당 오브젝트를 점령하고, 점령에 참여한 유닛이 Primordial Call 배틀 트레잇으로 소환된 유닛이면 이 배틀 택틱을 달성한다.";

const BeastofChaosTacticsKOR: oneTactic[] = [
  {
    id: 9,
    rank: 9,
    name: "Shadow of the HerdStone(kr)",
    description: sothsDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 10,
    rank: 10,
    name: "Fury of The Wild(kr)",
    description: FuryDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 11,
    rank: 11,
    name: "Wrath of the Warped Wilds(kr)",
    description: wrathDescriptionKOR,
    monsterBonus: false,
    result: "ongoing",
  },
];

export { BeastofChaosTactics, BeastofChaosTacticsfr, BeastofChaosTacticsKOR };
