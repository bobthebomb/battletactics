import { oneTactic } from "../interfaces/battletomeInterface";


const mineDescription = 'Pick 1 objective on the battlefield wholly outside of your territory. You complete this tactic if that objective marker is kicked away and is wholly within your territory at the end of this turn.'
const wreckingDescription = 'You complete this tactic if a faction terrain feature in your opponent’s army is demolished this turn.'
const manskittlesDescription = 'You complete this tactic if a friendly WARSTOMPER MEGA-GARGANT uses its ‘Hurled Body’ ability, the enemy model you pick for the first part of this ability is slain, and an enemy Battleline unit is picked as the target for the second part of the ability and suffers any mortal wounds as a result.'



const sonsOfBehematTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "That’s Mine!", description: mineDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Wrecking Crew", description: wreckingDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Manskittles", description: manskittlesDescription , monsterBonus: false, result : "ongoing" },
]

const mineDescriptionKOR  = '자신의 배치 구역 완전히 바깥에 위치한 오브젝트를 하나 선택한다. 이번 턴 안에 해당 오브젝트를 내 배치 구역 안으로 차 넣었다면 이 배틀 택틱을 달성한다.'
const wreckingDescriptionKOR  = '상대방 아미의 팩션 지형을 이번 턴 안에 파괴하면 이 배틀 택틱을 달성한다.'
const manskittlesDescriptionKOR  = '아군 WARSTOMPER MEGA-GARGANT가 Hurled Body 어빌리티를 사용해, 적 모델을 제거한 다음 후속 능력으로 상대방 배틀라인 유닛을 대상으로 지정해 모탈 운드 피해를 입히면 이 배틀 택틱을 달성한다.'



const sonsOfBehematTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "That’s Mine!(kr)", description: mineDescriptionKOR  , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Wrecking Crew(kr)", description: wreckingDescriptionKOR  , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Manskittles(kr)", description: manskittlesDescriptionKOR  , monsterBonus: false, result : "ongoing" },
]

export {sonsOfBehematTactics, sonsOfBehematTacticsKOR} ; 