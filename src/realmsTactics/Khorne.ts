import { oneTactic } from "../interfaces/battletomeInterface";


const bloodDescription = 'Pick 1 ennemy unit within 9" of your Skull Altar. You complete this battle tactic if that unit is destroyed during this turn.'
const slayDescription = 'Pick 1 ennemy HERO on the battlefield that is a WIZARD. You complete this battle tactic if that HERO is slain during this turn'
const trialOfSkullsDescription = 'Pick 1 friendly unit. You complete this battle tactic if 8 or more enemy models are slain by attacks made by that unit during this turn'


const khorneTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Blood for the Altar", description: bloodDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Slay the Sorcerer", description: slayDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "The Trial of Skulls", description:  trialOfSkullsDescription, monsterBonus: false, result : "ongoing" },
]


const bloodDescriptionKOR = '스컬 알타 9" 이내의 적 유닛을 하나 선택한다. 이번 턴 안에 해당 적 유닛을 파괴하면 이 배틀 택틱을 달성한다.'
const slayDescriptionKOR = '전장에 있는 WIZARD인 적 HERO를 하나 선택한다. 이번 턴 안에 해당 적 유닛을 하나 파괴하면 이 배틀 택틱을 달성한다.'
const trialOfSkullsDescriptionKOR = '아군 유닛을 하나 선택한다. 이번 턴 안에 적 모델을 8 이상 제거한다면, 이 배틀 택틱을 달성한다.'


const khorneTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Blood for the Altar(kr)", description: bloodDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Slay the Sorcerer(kr)", description: slayDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "The Trial of Skulls(kr)", description:  trialOfSkullsDescriptionKOR, monsterBonus: false, result : "ongoing" },
]

export {khorneTactics, khorneTacticsKOR} ; 