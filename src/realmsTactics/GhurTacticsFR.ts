import { oneTactic } from "../interfaces/battletomeInterface";


const conquerDescription = "Lorsque vous révélez cette tactique de bataille, choisissez 1 unité de Ligne de l’arméede départ adverse sur le champ de bataille. Vous accomplissez cette tactique de bataille si l’unitéchoisie est détruite pendant ce tour. Si elle est détruite par une attaque d’un Monstre ami ou uneaptitude d’un Monstre ami, marquez 1 PV supplémentaire."
const brokenRanksDescription = "Lorsque vous révélez cette tactique de bataille, choisissez 1 pion objectif sur le champ debataille contrôlée par l’adversaire. Vous accomplissez cette tactique de bataille si vous contrôlez lepion objectif choisi à la fin de ce tour."
const slayTheWarlordDescription = ": Vous accomplissez cette tactique de bataille si le général adverse est tuépendant ce tour. S’il est détruit par une attaque d’un Monstre ami ou une aptitude d’un Monstre ami,marquez 1 PV supplémentaire."
const ferociousAdvanceDescription = "Lorsque vous révélez cette tactique de bataille, choisissez 3 unités différentes devotre armée de départ sur le champ de bataille. Vous accomplissez cette tactique de bataille si toutes les unités choisies courent à la phase de mouvement suivantes et finissent cette course à 3' les unes des autres. Si les 3 unités sont toutes des Monstres, marquez 1 PV supplémentaire."
const bringItDownsDescription = "Lorsque vous révélez cette tactique de bataille, choisissez 1 unité Monstre de l’armée de départ adverse sur le champ de bataille. Vous accomplissez cette tactique de bataille si l’unité choisie est détruite pendant ce tour. Si elle est détruite par une attaque d’un Monstre ami ou une aptitude d’un Monstre ami, marquez 1 PV supplémentaire."
const agressiveExpansionDescription = "Expansion Agressive : Lorsque vous révélez cette tactique de bataille, choisissez 2 pions objectifs sur le champ de bataille qui ne sont pas entièrement dans votre territoire. Vous accomplissez cette tactique de bataille si vous contrôlez les 2 pions objectifs à la fin de ce tour."
const monstrousTakeOver = "Lorsque vous révélez cette tactique de bataille, choisissez 1 Monstre de votre armée de départ sur le champ de bataille. Vous accomplissez cette tactique de bataille si le Monstre choisi conteste un pion objectif que vous contrôlez à la fin du tour, et si le pion objectif n’est pas contesté par un Monstre ennemi."
const savageSpearHeadDescription = "Vous accomplissez cette tactique de bataille s’il y a au moins 2 unités de votre armée de départ qui sont entièrement dans le territoire adverse à la fin de ce tour. Si au moins deux de ses unités sont des Monstres, marquez 1 PV supplémentaire."



const ghurTacticsFR: oneTactic[] = [
    {id : 1 , rank: 1,  name : "Rangs Brisé", description: brokenRanksDescription, monsterBonus: true, result : "ongoing" },
    {id : 2 , rank: 2,  name : "Conquerir", description: conquerDescription , monsterBonus: false, result : "ongoing" },
    {id : 3 , rank: 3,  name : "Tuer le Seigneur de Guerre", description: slayTheWarlordDescription, monsterBonus: true, result : "ongoing" },
    {id : 4 , rank: 4, name : "Avance Féroce", description: ferociousAdvanceDescription, monsterBonus: true, result : "ongoing" },
    {id : 5 , rank: 5,  name : "Mise à Mort", description: bringItDownsDescription, monsterBonus: true, result : "ongoing" },
    {id : 6 , rank: 6, name : "Expansion Agressive", description: agressiveExpansionDescription, monsterBonus: false, result : "ongoing" },
    {id : 7 , rank: 7,  name : "Monstre Accapareur", description: monstrousTakeOver, monsterBonus: false, result : "ongoing" },
    {id : 8 , rank: 8, name : "Fer de Lance Sauvage", description: savageSpearHeadDescription, monsterBonus: true , result : "ongoing" },
]

export default ghurTacticsFR;

