import { oneTactic } from "../interfaces/battletomeInterface";


const conquerDescription = "이 배틀 택틱을 공개할 때, 상대방이 점령하고 있는 전장의 오브젝트 마커를 하나 선택한다. 이번 턴이 끝날 때 해당 오브젝트를 점령하고 있다면 배틀 택틱을 달성한다."
const brokenRanksDescription = "이 배틀 택틱을 공개할 때, 상대방 시작 아미에 포함된 배틀라인 유닛을 하나 선택한다. 이번 턴 안에 해당 유닛이 파괴되면 이 배틀택틱을 달성한다. 만약 해당 유닛이 아군 MONSTER의 공격이나 어빌리티로 파괴되었다면, 추가로 승점을 1점 획득한다."
const slayTheWarlordDescription = "이 배틀 택틱은 이번 턴 안에 상대방의 제너럴 모델을 제거하면 달성한다. 만약 해당 유닛이 아군 MONSTER의 공격이나 어빌리티로 파괴되었다면, 추가로 승점을 1점 획득한다. "
const ferociousAdvanceDescription = '이 배틀 택틱을 공개할 때, 전장에 있는 시작 아미에 포함된 서로 다른 3 유닛을 선택한다. 해당 유닛들이 모두 런을 했고, 이동을 마친후 서로 3" 이내에 있다면 이 배틀 택틱을 달성한다. 만약, 3 유닛이 전부 다 MONSTER 유닛이라면, 추가로 승점을 1점 획득한다.'
const bringItDownsDescription = "이 배틀 택틱을 공개할 때, 전장에 있는 상대방 MONSTER를 하나 선택한다. 이번 턴 안에 해당 유닛을 파괴하면 이 배틀 택틱을 달성한다. 만약 해당 유닛이 아군 MONSTER의 공격이나 어빌리티로 파괴되었다면, 추가로 승점을 1점 획득한다."
const agressiveExpansionDescription = "이 배틀 택틱을 공개할 때, 자신의 배치 구역안에 완전히 들어가 있지 않은 전장의 오브젝트 마커를 2개 선택한다. 이번 턴이 끝날 때 해당 오브젝트를 모두 점령하고 있다면 배틀 택틱을 달성한다."
const monstrousTakeOver = "이 배틀 택틱을 공개할 때, 전장의 자신의 시작 아미에 속한 MONSTER를 하나 선택한다. 이번 턴이 끝날 때 해당 MONSTER가 자신이 점령하고 있는 오브젝트를 점거중이고, 상대방 MONSTER와 경쟁중이지 않는다면 배틀 택틱을 달성한다."
const savageSpearHeadDescription = "이번 턴이 종료할 때, 시작 아미에 속한 2 이상의 유닛이 상대방 배치 구역에 완전히 들어가면 이 배틀 택틱을 달성한다. 만약 2 이상의 유닛들이 MONSTER라면, 추가로 승점을 1점 획득한다."



const ghurTacticsKOR: oneTactic[] = [
    {id : 1 , rank: 1,  name : "Broken Ranks(kr)", description: brokenRanksDescription, monsterBonus: true, result : "ongoing" },
    {id : 2 , rank: 2,  name : "Conquer(kr)", description: conquerDescription , monsterBonus: false, result : "ongoing" },
    {id : 3 , rank: 3,  name : "Slay the Warlord(kr)", description: slayTheWarlordDescription, monsterBonus: true, result : "ongoing" },
    {id : 4 , rank: 4, name : "Ferocious Advance(kr)", description: ferociousAdvanceDescription, monsterBonus: true, result : "ongoing" },
    {id : 5 , rank: 5,  name : "Bring it Down(kr)", description: bringItDownsDescription, monsterBonus: true, result : "ongoing" },
    {id : 6 , rank: 6, name : "Agressive Expension(kr)", description: agressiveExpansionDescription, monsterBonus: false, result : "ongoing" },
    {id : 7 , rank: 7,  name : "Monstrous Take Over(kr)", description: monstrousTakeOver, monsterBonus: false, result : "ongoing" },
    {id : 8 , rank: 8, name : "Savage Spearhead(kr)", description: savageSpearHeadDescription, monsterBonus: true , result : "ongoing" },
]

export default ghurTacticsKOR;

