import { oneTactic } from "../interfaces/battletomeInterface";


const mightOfTheStarbornDescription = 'Pick 1 objective on the battlefield. You complete this battle tactic if you summon a friendly unit that has a CCP cost of 20 or more during the turn, and that unit is wholly within 12" of the objective you picked'
const stampedeDescription = 'Pick 3 different friendly MONSTER. You complete this battle tactic if all of the units you picked run in the following movement phase and finish that run within 6" of each other and wholly within enemy territory'


const seraphonTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Might of the Starborne", description: mightOfTheStarbornDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Stampede of Scale", description: stampedeDescription , monsterBonus: false, result : "ongoing" },
]


const mightOfTheStarbornDescriptionKOR = '전장의 오브젝트 하나를 선택한다. CCP 20점 이상의 유닛을 소환하여, 선택한 오브젝트 12" 이내에 완전히 들어가게끔 하면 달성.'
const stampedeDescriptionKOR = '각기 다른 아군 MONSTER를 3개 선택한다. 선택한 모든 유닛이 이번 이동 페이즈에 런 이동을 하고, 서로 6" 이내에 있으며, 적 배치구역에 완전히 들어가면 달성'


const seraphonTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Might of the Starborne(kr)", description: mightOfTheStarbornDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Stampede of Scale(kr)", description: stampedeDescriptionKOR , monsterBonus: false, result : "ongoing" },
]

export {seraphonTactics, seraphonTacticsKOR} ; 