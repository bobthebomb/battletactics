import { oneTactic } from "../interfaces/battletomeInterface";


const thrallDescription = 'Pick on objective on the battlefield within 12" of any ennemy units. You complete this battle tactic if there are no enemy units within 12" of that objective at the end of this turn. '
const LustDescription = 'Pick 1 friendly SLAVES TO DARKNESS HERO that has the EYE OF THE GODS keywords. You complete this battle tactic if you roll on the Eye of the Gods table for that Hero during this turn'


const slavesToDarknessTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "In Thrall to Chaos", description: thrallDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Lust of power", description: LustDescription , monsterBonus: false, result : "ongoing" },
]


const thrallDescriptionKOR = '적 유닛 12" 이내의 오브젝트를 하나 선택한다. 이 배틀 택틱은 이번 턴이 끝날 때, 해당 오브젝트 12" 주변에 적 유닛이 없으면 달성한다.'
const LustDescriptionKOR = 'EYE OF THE GODS 키워드를 가진 아군 SLAVES TO DARKNESS HERO 히어로를 하나 선택한다. 이 배틀 택틱은 이번 턴 동안, 해당 히어로가 Eye of the Gods 테이블에서 주사위를 굴리면 달성한다.'


const slavesToDarknessTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "In Thrall to Chaos(kr)", description: thrallDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Lust of power(kr)", description: LustDescriptionKOR , monsterBonus: false, result : "ongoing" },
]


export {slavesToDarknessTactics, slavesToDarknessTacticsKOR} ; 