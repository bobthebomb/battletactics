import { oneTactic } from "../interfaces/battletomeInterface";

const reprisalDescription =
  "You complete this battle tactic if an enemy unit that destroyed a friendly general earlier in the battle is destroyed this turn";
const intimidateTheInvadersDescription =
  " You complete this battle tactic at the end of your turn if there are more friendly units wholly outside your territory than there are friendly units within your territory";
const endlessExpriopriationDescription = `Pick 1 enemy unit that is controlling or bonded to an endless spell or INCARNATE.
You complete this battle tactic at the end of your turn if any of the following are true : 
That enemy unit has been destroyed.
That endless spell is wild.
That endless spell is controlled or bound to a friendly unit.
That INCARNATE is wild.`;
const magicalDominanceDescription = `You complete this battle tactic at the end of your turn if a friendly WIZARD unit successfully casts 1 or more spells and none of the spells cast by any units in your army were unbound`;

const MagicalMayhemDescription = `Pick 1 enemy unit on the battlefield. You complete this battle tactic if that unit is destroyed by damage inflicted by a spell or the abilities of an endless spell`;
const baitAndTrapDescription = `You complete this battle tactic if 2 or more friendly units retreated this turn and 2 or more different friendly units made a charge move this turn.`;
const ledIntoTheMaelstromDescription = `You complete this battle tactic if 1 or more friendly HEROES and 1 or more friendly Battleline units each made a charge move this turn and at least 1 of those units is within 3" of an enemy unit at the end of the turn`;
const surroundAndDestroyDescription = `Pick 3 different friendly units on the battlefield. You complete this battle
tactic at the end of your turn if each of those units is wholly within 6" of a different battlefield edge and 2 or more of those units are wholly outside your territory`;

const GHB2023Tactics: oneTactic[] = [
  {
    id: 1,
    rank: 1,
    name: "Intimidate the Invaders",
    description: intimidateTheInvadersDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 2,
    rank: 2,
    name: "Reprisal",
    description: reprisalDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 3,
    rank: 3,
    name: "Endless Expropriation",
    description: endlessExpriopriationDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 4,
    rank: 4,
    name: "Magical Dominance",
    description: magicalDominanceDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 5,
    rank: 5,
    name: "Magical Mayhem",
    description: MagicalMayhemDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 6,
    rank: 6,
    name: "Bait and Trap",
    description: baitAndTrapDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 7,
    rank: 7,
    name: "Led into the Maelstrom",
    description: ledIntoTheMaelstromDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 8,
    rank: 8,
    name: "Surround and Destroy",
    description: surroundAndDestroyDescription,
    monsterBonus: false,
    result: "ongoing",
  },
];

export default GHB2023Tactics;
