import { oneTactic } from "../interfaces/battletomeInterface";


const assassinDescription = 'You complete this battle tactic if 2 or more enemy units are destroyed during this turn by attacks made by friendly IDONETH DEEPKIN units that are affected by the HIGH TIDE ability'
const predatorsDescription = 'Pick 1 unit in your opponents army that is on the battlefield, that has a wounds characteristic of 8 or more and that has 0 wounds allocated to it. You complete this tactic if that unit is destroyed during this turn by a friendly AKHELIAN ALLOPEXES unit'
const revengeDescription = 'You complete this battle tactic if an enemy HERO or Monster is destroyed during this turn by an attack made by a friendly NAMARTI unit'
const denyDescription = 'Pick one Gloomtide Shipwreck in your army that is within 12" of any enemy units. You complete this tactif if that Gloomtide Shipwreck is more thant 12" from all enemy units at the end of this turn'
const trappedDescription = 'You complete this battle tactic if 3 or more friendly IDONETH DEEPKIN units retreated and made a charge move during this turn'
const isharannDescription = 'When you reveal this battle tactics, pick 1 objective wholly within enemy territory. At the end of this turn, you complete this battle tactic if you control that objective and there is a friendly ISHARANN unit within 6" of that objective'



const idonethTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Assassin of the High Tide", description: assassinDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Predators of the Deep", description: predatorsDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Revenge of the Namarti", description: revengeDescription , monsterBonus: false, result : "ongoing" },
    {id : 12 , rank: 12,  name : "Deny Trespassers", description: denyDescription , monsterBonus: false, result : "ongoing" },
    {id : 13 , rank: 13,  name : "Trapped in the Undercurrents", description: trappedDescription , monsterBonus: false, result : "ongoing" },
    {id : 14 , rank: 14,  name : "Isharann Defiance", description: isharannDescription , monsterBonus: false, result : "ongoing" },
]

const assassinDescriptionKOR = '이번 턴 안에 2 이상의 적 유닛이 HIGH TIDE의 효과를 받는 IDONETH DEEPKIN 유닛의 공격으로 파괴되면, 이 배틀 택틱을 달성한다.'
const predatorsDescriptionKOR = '전장에 있는 상대방 아미의 아직 피해를 입지 않았으면서, 8 운드 이상인 유닛을 하나 선택한다. 이번 턴 안에 아군 AKHELIAN ALLOPEXES 유닛에 의해 해당 유닛이 파괴되면 이 배틀 택틱을 달성한다.'
const revengeDescriptionKOR = '이번 턴 안에 아군 NAMARTI 유닛이 가한 공격에 의해 적 HERO나 MONSTER가 파괴되면, 이 배틀 택틱을 달성한다.'
const denyDescriptionKOR = '12" 이내에 적 유닛이 있는 자신의 아미에 속한 Gloomtide Shipwreck를 하나 선택한다. 이번 턴이 끝날 때, 해당 Gloomtide Shipwreck 12" 이내에 적 유닛이 없으면 이 배틀 택틱을 달성한다.'
const trappedDescriptionKOR = '이번 턴에 3 이상의 아군 IDONETH DEEPKIN 유닛이 리트릿 이동을 하고 차지 이동을 했다면, 이 배틀 택틱을 달성한다.'
const isharannDescriptionKOR = '이 배틀 택틱을 공개할 때, 상대방의 영역에 완전히 들어간 오브젝트를 1개 선택한다. 이번 턴이 끝날 때, 해당 오브젝트를 점령하고, 6" 이내에 아군 ISHARANN 유닛이 있다면, 이 배틀 택틱을 달성한다.'



const idonethTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Assassin of the High Tide(kr)", description: assassinDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Predators of the Deep(kr)", description: predatorsDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Revenge of the Namarti(kr)", description: revengeDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 12 , rank: 12,  name : "Deny Trespassers(kr)", description: denyDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 13 , rank: 13,  name : "Trapped in the Undercurrents(kr)", description: trappedDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 14 , rank: 14,  name : "Isharann Defiance(kr)", description: isharannDescriptionKOR , monsterBonus: false, result : "ongoing" },
]


    export {idonethTactics, idonethTacticsKOR} ;