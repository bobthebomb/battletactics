import GhurTacticsEN from "./GhurTacticsEN";
import GhurTacticsFR from "./GhurTacticsFR";
import GHB2023Tactics from "./GHB2023";
import {
  BeastofChaosTactics,
  BeastofChaosTacticsfr,
  BeastofChaosTacticsKOR,
} from "./BeastOfChaos";
import {
  soulblightGravelordsTactics,
  soulblightGravelordsTacticsKOR,
} from "./SoulblightGravelords";
import { ossiarchTactics, ossiarchTacticsKOR } from "./Ossiarch";
import { citiesTactics, citiesTacticsKOR } from "./CitiesOfSigmar";
import {
  slavesToDarknessTactics,
  slavesToDarknessTacticsKOR,
} from "./SlavesToDarkness";
import { seraphonTactics, seraphonTacticsKOR } from "./seraphon";
import { ironJawzTactics, ironJawzTacticsKOR } from "./IronJawz";
import { kruleBoysTactics, kruleBoysTacticsKOR } from "./KruleBoys";
import { bigWaghTactics, bigWaghTacticsKOR } from "./BigWagh";
import { bonesSplitersTactic, bonesSplitersTacticKOR } from "./BonesSplitters";
import {
  stormcastEternalsTactics,
  stormcastEternalsTacticsKOR,
} from "./Stormcast";
import { sonsOfBehematTactics, sonsOfBehematTacticsKOR } from "./SonsOfBehemat";
import { maggotKinTactics, maggotKinTacticsKOR } from "./Maggotkin";
import { idonethTactics, idonethTacticsKOR } from "./Idoneth";
import { fireslayersTactics, fireslayersTacticsKOR } from "./Fireslayers";
import { khorneTactics, khorneTacticsKOR } from "./Khorne";
import ghurTacticsKOR from "./GhurTacticsKR";
import { dokTactics as dokTacticsBook } from "./DOK";

interface MenuList {
  val: string;
  name: string;
}

// il faut que l'indicatif de langue soit le meme que la key string de l'objet

const objOfTacticsVersion = {
  ENG: GHB2023Tactics,
  // FR: GhurTacticsFR,
  // KOR: ghurTacticsKOR,
};

const dokTactics = {
  DOKENG: dokTacticsBook,
  DOKFR: dokTacticsBook,
  DOKKOR: dokTacticsBook,
};

const bocTactics = {
  BOCENG: BeastofChaosTactics,
  BOCFR: BeastofChaosTacticsfr,
  BOCKOR: BeastofChaosTacticsKOR,
};

const sbglTactics = {
  SBGLENG: soulblightGravelordsTactics,
  SBGLFR: soulblightGravelordsTactics,
  SBGLKOR: soulblightGravelordsTacticsKOR,
};

const obrTactics = {
  OBRENG: ossiarchTactics,
  OBRFR: ossiarchTactics,
  OBRKOR: ossiarchTacticsKOR,
};

const cosTactics = {
  COSENG: citiesTactics,
  COSFR: citiesTactics,
  COSKOR: citiesTacticsKOR,
};

const stdTactics = {
  STDENG: slavesToDarknessTactics,
  STDFR: slavesToDarknessTactics,
  STDKOR: slavesToDarknessTacticsKOR,
};

const lizTactics = {
  LIZENG: seraphonTactics,
  LIZFR: seraphonTactics,
  LIZKOR: seraphonTacticsKOR,
};

const ijTactics = {
  IJENG: ironJawzTactics,
  IJFR: ironJawzTactics,
  IJKOR: ironJawzTacticsKOR,
};

const kbTactics = {
  KBENG: kruleBoysTactics,
  KBFR: kruleBoysTactics,
  KBKOR: kruleBoysTacticsKOR,
};

const bwTactics = {
  BWENG: bigWaghTactics,
  BWFR: bigWaghTactics,
  BWKOR: bigWaghTacticsKOR,
};

const bsTactics = {
  BSENG: bonesSplitersTactic,
  BSFR: bonesSplitersTactic,
  BSKOR: bonesSplitersTacticKOR,
};

const sceTactics = {
  SCEENG: stormcastEternalsTactics,
  SCEFR: stormcastEternalsTactics,
  SCEKOR: stormcastEternalsTacticsKOR,
};

const sonTactics = {
  SONSENG: sonsOfBehematTactics,
  SONSFR: sonsOfBehematTactics,
  SONSKOR: sonsOfBehematTacticsKOR,
};

const monTactics = {
  MONENG: maggotKinTactics,
  MONFR: maggotKinTactics,
  MONKOR: maggotKinTacticsKOR,
};

const idoTactics = {
  IDOENG: idonethTactics,
  IDOFR: idonethTactics,
  IDOKOR: idonethTacticsKOR,
};

const fsTactics = {
  FSENG: fireslayersTactics,
  FSFR: fireslayersTactics,
  FSKOR: fireslayersTacticsKOR,
};

const bokTactics = {
  BOKENG: khorneTactics,
  BOKFR: khorneTactics,
  BOKKOR: khorneTacticsKOR,
};

const battleTacticsBattletome = {
  ...bocTactics,
  ...sbglTactics,
  ...obrTactics,
  ...cosTactics,
  ...stdTactics,
  ...lizTactics,
  ...ijTactics,
  ...kbTactics,
  ...bwTactics,
  ...bsTactics,
  ...sceTactics,
  ...sonTactics,
  ...monTactics,
  ...idoTactics,
  ...fsTactics,
  ...bokTactics,
  ...dokTactics,
};

const battletomeMenuList: MenuList[] = [
  { val: "BOC", name: "Beast of Chaos" },
  { val: "BS", name: "Bones Splitters" },
  { val: "BW", name: "Big Waaagh" },
  { val: "COS", name: "Cities of Sigmar" },
  { val: "FS", name: "Fireslayers" },
  { val: "IDO", name: "Idoneth Deepkin" },
  { val: "IJ", name: "IronJawz" },
  { val: "KB", name: "KrulleBoys" },
  { val: "MON", name: "MaggotKin of Nurgles" },
  { val: "OBR", name: "Ossiarch Bonereapers" },
  { val: "LIZ", name: "Seraphon" },
  { val: "SBGL", name: "Soulblight Gravelords" },
  { val: "SONS", name: "Sons of Behemat" },
  { val: "STD", name: "Slaves to Darkness" },
  { val: "SCE", name: "Stormcast Eternals" },
  { val: "BOK", name: "Blades of Khorne" },
  { val: "DOK", name: "Daughter of Khaine" },
];

export { objOfTacticsVersion, battleTacticsBattletome, battletomeMenuList };
