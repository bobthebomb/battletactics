import { oneTactic } from "../interfaces/battletomeInterface";


const timeToGetStuckInDescription = 'You can pick this battle tactic only in your first or second turn. You complete this tactic if the model picked to be your general and all of the models in your army that are on the battlefield are within 12" of an enemy unit at the end of this turn.'
const takeDateDescription = 'You can pick this battle tactic only if the model picked to be your general has the KRULEBOYZ keyword. You complete this tactic if the following 2 criteria are met:- At least 10 wounds or mortal wounds in any combination that were caused by friendly units are allocated to enemy models this turn.- Fewer than 10 wounds or mortal wounds in any combination that were caused by enemy units are allocated to friendly models this turn.'
const destroyerDescription = 'You can pick this battle tactic only if a friendly KRAGNOS is on the battlefield. Pick 1 faction terrain feature on the battlefield that was set up by your opponent and that has not been demolished. You complete this tactic if that faction terrain feature is demolished this turn.'


const kruleBoysTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Time to Get Stuck In", description: timeToGetStuckInDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Take Dat, Ya Suckers!", description: takeDateDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Destroyer of Empires(kb)", description:  destroyerDescription, monsterBonus: false, result : "ongoing" },
]

const timeToGetStuckInDescriptionKOR = '이 배틀택틱은 오직 자신의 1, 2턴에만 사용할 수 있다. 이번 턴이 끝날 때 제너럴로 선택한 모델과 자신의 아미에 속한 모든 아군 모델들이 적 유닛 12" 이내에 있다면, 이 배틀 택틱을 달성한다.'
const takeDateDescriptionKOR = '이 배틀 택틱은 제너럴로 선택된 모델이 KRULEBOYZ 키워드를 가지고 있을때만 사용할 수 있다. 다음 두가지 조건을 만족하면 배틀 택틱을 달성한다. - 이번 턴 안에 적 모델에게 아군 유닛에게 최소 10의 운드, 모탈 운드 피해를 입히고 - 이번 턴 안에 적 유닛이 아군 유닛에게 10보다 적은 운드, 모탈 운드 피해를 입혔다면'
const destroyerDescriptionKOR = '전장에 아군 KRAGNOS가 있을 때만 이 배틀 택틱을 선택할 수 있다. 전장의 상대방이 배치한 아직 파괴되지 않은 팩션 지형을 하나 선택한다. 이번 턴 안에 해당 지형이 파괴되면 이 배틀 택틱을 달성한다.'


const kruleBoysTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Time to Get Stuck In(kr)", description: timeToGetStuckInDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Take Dat, Ya Suckers!(kr)", description: takeDateDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Destroyer of Empires(kb)(kr)", description:  destroyerDescriptionKOR, monsterBonus: false, result : "ongoing" },
]

export {kruleBoysTactics, kruleBoysTacticsKOR} ; 