import { oneTactic } from "../interfaces/battletomeInterface";

const conquerDescription =
  "When you reveal this battle tactic, pick 1 objective marker on the battlefield that your opponent controls. You complete this battle tactic if you control that objective marker at the end of this turn.";
const brokenRanksDescription =
  "When you reveal this battle tactic, pick 1 Battleline unit from your opponent’s starting army on the battlefield. You complete this battle tactic if that unit is destroyed during this turn. If that unit is destroyed by an attack made by a friendly MONSTER or an ability of a friendly MONSTER, score 1 additional victory point.";
const slayTheWarlordDescription =
  "You complete this battle tactic if the model chosen to be your opponent’s general is slain during this turn. If that model was destroyed by a friendly MONSTER or an ability of a friendly MONSTER, score 1 additional victory point.";
const ferociousAdvanceDescription =
  "When you reveal this battle tactic, pick 3 different units from your starting army on the battlefield. You complete this battle tactic if all of the units you picked run in the following movement phase and finish that run within 3″ of each other. If all 3 of those units are MONSTERS, score 1 additional victory point.";
const bringItDownsDescription =
  "When you reveal this battle tactic, pick 1 enemy MONSTER on the battlefield. You complete this battle tactic if that unit is destroyed during this turn. If that enemy MONSTER was destroyed by an attack made by a friendly MONSTER or an ability off a friendly MONSTER, score 1 additional victory point.";
const agressiveExpansionDescription =
  "When you reveal this battle tactic, pick 2 objective markers on the battlefield that are not wholly within your territory. You complete this battle tactic if you control both objective markers at the end of this turn.";
const monstrousTakeOver =
  "When you reveal this battle tactic, pick 1 MONSTER from your starting army on the battlefield. You complete this battle tactic if that MONSTER is contesting an objective marker that you control at the end of this turn, and that objective marker is not contested by an enemy MONSTER.";
const savageSpearHeadDescription =
  "You complete this battle tactic if there are 2 or more units from your starting army wholly within your opponent’s territory at the end of this turn. If 2 or more of those units are MONSTERS, score 1 additional victory point.";

const ghurTactics: oneTactic[] = [
  {
    id: 1,
    rank: 1,
    name: "Broken Ranks",
    description: brokenRanksDescription,
    monsterBonus: true,
    result: "ongoing",
  },
  {
    id: 2,
    rank: 2,
    name: "Conquer",
    description: conquerDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 3,
    rank: 3,
    name: "Slay the Warlord",
    description: slayTheWarlordDescription,
    monsterBonus: true,
    result: "ongoing",
  },
  {
    id: 4,
    rank: 4,
    name: "Ferocious Advance",
    description: ferociousAdvanceDescription,
    monsterBonus: true,
    result: "ongoing",
  },
  {
    id: 5,
    rank: 5,
    name: "Bring it Down",
    description: bringItDownsDescription,
    monsterBonus: true,
    result: "ongoing",
  },
  {
    id: 6,
    rank: 6,
    name: "Agressive Expansion",
    description: agressiveExpansionDescription,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 7,
    rank: 7,
    name: "Monstrous Take Over",
    description: monstrousTakeOver,
    monsterBonus: false,
    result: "ongoing",
  },
  {
    id: 8,
    rank: 8,
    name: "Savage Spearhead",
    description: savageSpearHeadDescription,
    monsterBonus: true,
    result: "ongoing",
  },
];

export default ghurTactics;
