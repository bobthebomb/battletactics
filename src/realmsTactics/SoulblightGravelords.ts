import { oneTactic } from "../interfaces/battletomeInterface";


const overlordDescription = 'Pick 1 friendly SUMMONABLE unit that is more thant 3" from all enemy units. You complete this battle tactic if that friendly unit is destroyed during this turn'
const lustDescription = 'You complete this battle tactics if your general or two other friendly VAMPIRE models used the Hunger and/or Mortarch of the Blood ability to heal any wounds during this turn'
const unstopableDescription = 'You complete this battle tactic if, during this turn, you roll a 5+ when determining if you can replace a destroyed unit from your army using the Endless Legions battle trait'


const soulblightGravelordsTactics: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Callous Overlord", description: overlordDescription , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Lust for blood", description: lustDescription , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Unstopable armies", description: unstopableDescription , monsterBonus: false, result : "ongoing" },
]

const overlordDescriptionKOR = '적 3" 바깥의 아군 SUMMONABLE 유닛을 하나 선택한다. 해당 유닛이 이번 턴 안에 파괴된다면 배틀 택틱을 달성한다.'
const lustDescriptionKOR = '아군 제네럴이나 2 이상의 서로 다른 VAMPIRE 모델이 Hunger/Mortarch of Blood의 효과로 피해를 회복하면 달성한다.'
const unstopableDescriptionKOR = '이번 턴 안에, 파괴된 유닛을 Endless Legion 배틀 트레잇을 사용해 굴림값 5+ 이상으로 부활시키면 달성한다'


const soulblightGravelordsTacticsKOR: oneTactic[] = [
    {id : 9 , rank: 9,  name : "Callous Overlord(kr)", description: overlordDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 10 , rank: 10,  name : "Lust for blood(kr)", description: lustDescriptionKOR , monsterBonus: false, result : "ongoing" },
    {id : 11 , rank: 11,  name : "Unstopable armies(kr)", description: unstopableDescriptionKOR , monsterBonus: false, result : "ongoing" },
]

export {soulblightGravelordsTactics, soulblightGravelordsTacticsKOR} ; 