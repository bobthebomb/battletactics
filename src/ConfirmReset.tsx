import React, { useContext } from "react";
import { Button } from "@mui/material";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
// import { BattleTactics } from "./contexts/battleTactics.context";
const { BattleTactics } = require("./contexts/battleTactics.context");

interface ConfirmResetProps {
  confirm: boolean;
  setConfirm: Function;
}

export default function ConfirmReset(props: ConfirmResetProps): JSX.Element {
  const { dispatch } = useContext(BattleTactics);
  // open and setopen methods
  const { confirm, setConfirm } = props;

  const handleClickReset = () => {
    setConfirm();
    dispatch({ type: "Reset" });
  };

  return (
    <Dialog
      open={confirm}
      onClose={() => setConfirm()}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Confirm ?</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Confirm the deletion of Battle Tactics, Battletome choice will reset
          as well
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setConfirm()}>Disagree</Button>
        <Button onClick={handleClickReset} autoFocus>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
}
