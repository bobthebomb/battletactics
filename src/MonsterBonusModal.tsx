import React from "react";
import Modal from "@mui/material/Modal";
import { Typography, Box } from "@mui/material";
import PlusOneRoundedIcon from "@mui/icons-material/PlusOneRounded";

type MonsterBonusModalProps = {
  open: boolean;
  handleClose: Function;
  name: string;
};

function MonsterBonusModal(props: MonsterBonusModalProps): JSX.Element {
  const open = props.open;
  const handleClose = props.handleClose;

  const style = {
    position: "absolute",
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "60%",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Modal
      open={open}
      onClose={() => handleClose()}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          Monster Bonus Points{" "}
          <PlusOneRoundedIcon
            sx={{ border: "1px solid blue", color: "blue" }}
          />
        </Typography>
        <Typography
          id="modal-modal-description"
          variant="caption"
          sx={{ mt: 2 }}
        >
          You can get one extra victory point if you complete the {props.name}{" "}
          with a specific unit (Most of the time a Monster but not always )
        </Typography>
      </Box>
    </Modal>
  );
}
export default MonsterBonusModal;
